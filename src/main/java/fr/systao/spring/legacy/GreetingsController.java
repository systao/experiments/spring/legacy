package fr.systao.spring.legacy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GreetingsController {
	
	@Autowired
	private GreetingsService service;
	
	@ResponseBody
	@RequestMapping(path="/greetings", method=RequestMethod.GET)
	public String greetings() {
		return service.greetings();
	}
	
}
