package fr.systao.spring.legacy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:/application.properties")
public class GreetingsService {

	@Autowired
	private LegacyService legacyService;
	
	@Value("${global.name}")
	private String name;
	
	public String greetings() {
		return String.join("", "Hello, ", legacyService.getName(), " ", name);
	}
	
}
